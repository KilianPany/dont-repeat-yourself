package at.hakwt.swp4.dry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIActionListener implements ActionListener {

    private int count;

    @Override
    public void actionPerformed(ActionEvent e) {

        count = count + 10;
    }

    public int getCount() {

        return count;
    }
}